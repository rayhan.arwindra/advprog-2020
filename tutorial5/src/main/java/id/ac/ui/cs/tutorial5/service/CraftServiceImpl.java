package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class CraftServiceImpl implements CraftService {

    private String[] itemName = {
            "Dragon Breath", "Void Rhapsody", "Determination Symphony",
            "Opera of Wasteland", "FIRE BIRD"
    };

    private  List<CraftItem> allItems = new ArrayList<>();
    private CompletableFuture<Void> async;

    @Override
    public CraftItem createItem(String itemName) throws InterruptedException, ExecutionException{
        CraftItem craftItem;
        switch (itemName) {
            case "Dragon Breath":
                craftItem = createDragonBreath();
                break;
            case "Void Rhapsody":
                craftItem = createVoidRhapsody();
                break;
            case "Determination Symphony":
                craftItem = createDeterminationSymphony();
                break;
            case "Opera of Wasteland":
                craftItem = createOperaOfWasteland();
                break;
            case "FIRE BIRD":
                craftItem = createFireBird();
                break;
            default:
                craftItem = createFireBird();
        }
        allItems.add(craftItem);
        return craftItem;
    }

    private void composeMessage(CraftItem item){
        item.composeRecipes();
    }
    @Async
    private CraftItem createDragonBreath(){
        CraftItem dragonBreath = new CraftItem("Dragon Breath");
        async = CompletableFuture.runAsync(() -> dragonBreath.addRecipes(new SilentField()));
        async.thenRunAsync(() -> dragonBreath.addRecipes(new FireCrystal()));
        async.thenRun(()->composeMessage(dragonBreath));

        return dragonBreath ;
    }
    @Async
    private CraftItem createVoidRhapsody(){
        CraftItem voidRhapsody = new CraftItem("Void Rhapsody");
        async = CompletableFuture.runAsync(() -> voidRhapsody.addRecipes(new SilentField()));
        async.thenRunAsync(() -> voidRhapsody.addRecipes(new PureWaterfall()));
        async.thenRun(()->composeMessage(voidRhapsody));

        return voidRhapsody ;
    }
    @Async
    private CraftItem createDeterminationSymphony(){
        CraftItem determinationSymphony = new CraftItem("Determination Symphony");
        async = CompletableFuture.runAsync(() -> determinationSymphony.addRecipes(new BlueRoseRainfall()));
        async.thenRunAsync(() -> determinationSymphony.addRecipes(new PureWaterfall()));
        async.thenRun(()->composeMessage(determinationSymphony));

        return determinationSymphony;
    }
    @Async
    private CraftItem createOperaOfWasteland() {
        CraftItem wasteland = new CraftItem("Opera of Wasteland");
        async = CompletableFuture.runAsync(() -> wasteland.addRecipes(new DeathSword()));
        async.thenRunAsync(() -> wasteland.addRecipes(new SilentField()));
        async.thenRunAsync(()-> wasteland.addRecipes(new FireCrystal()));
        async.thenRun(()->composeMessage(wasteland));
        return wasteland;
    }
    @Async
    private CraftItem createFireBird(){
        CraftItem fireBird =  new CraftItem("FIRE BIRD");
        async = CompletableFuture.runAsync(() -> fireBird.addRecipes(new BirdEggs()));
        async.thenRunAsync(() -> fireBird.addRecipes(new FireCrystal()));
        async.thenRun(()->composeMessage(fireBird));
        return fireBird;
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
