package id.ac.ui.cs.tutorial5.controller;

import id.ac.ui.cs.tutorial5.service.CraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;

@Controller
@RequestMapping("/craft")
public class CraftController {

    @Autowired
    private CraftService craftService;

    @GetMapping("/")
    public String craftItemHome(Model model) {
        model.addAttribute("itemNames", craftService.getItemNames());
        model.addAttribute("craftItems", craftService.findAll());
        return "craft";
    }

    @PostMapping("/create")
    public String craftNewItem(HttpServletRequest request) throws ExecutionException, InterruptedException {
        String itemName = request.getParameter("itemName");
        craftService.createItem(itemName);
        return "redirect:/craft/";
    }
}
