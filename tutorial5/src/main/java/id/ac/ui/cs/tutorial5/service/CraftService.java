package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.CraftItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface CraftService {

    public CraftItem createItem(String itemName) throws InterruptedException, ExecutionException;
    public String[] getItemNames();
    public List<CraftItem> findAll();
}
