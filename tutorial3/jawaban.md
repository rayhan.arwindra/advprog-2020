1. The program first creates a thread in manalithServiceImpl such that the program runs quicker. Then, users can create a synth in the website. The synth can be a knight which takes mana, and a catalyst which creates it. Then, the user can push the process request button, which then calls manalithService.getSynthesis() and manalithService.processRequest() methods, and returns the info of each synth and their respective mana.

2. The program creates two threads which run the runManalith method, which then calls a method in the Manalith class called processRequest. The method then polls an item from a queue, and the race condition occurs because the two threads poll from this queue.

3. The race condition occurs here because the two threads run concurrently and have access to a queue, and queues are not thread safe. The two threads that run concurrently then polls from the queue, and the race condition occurs.

4. Since the queue is not thread safe, we should then change the queue to be a thread safe queue. One interface for a thread safe queue is a BlockingQueue. So changing the queue to a BlockingQueue should fix the race condition issue.