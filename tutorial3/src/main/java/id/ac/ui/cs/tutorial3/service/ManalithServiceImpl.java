package id.ac.ui.cs.tutorial3.service;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import id.ac.ui.cs.tutorial3.core.Manalith;
import id.ac.ui.cs.tutorial3.core.Synthesis;
import id.ac.ui.cs.tutorial3.repository.SynthesisRepository;
import org.springframework.stereotype.Service;

@Service
public class ManalithServiceImpl implements ManalithService {
	private final SynthesisRepository synthRepo;
	private final Manalith manalith;

	public ManalithServiceImpl(SynthesisRepository synthRepo, Manalith manalith){
		this.synthRepo = synthRepo;
		this.manalith = manalith;
	}

	public ManalithServiceImpl(SynthesisRepository synthRepo){
		this(synthRepo, new Manalith());
	}

	public ManalithServiceImpl(){
		this(new SynthesisRepository(), new Manalith());
	}

	@Override
	public void addSynth(Synthesis synth){
		synthRepo.addSynthesis(synth);
	}

	@Override
	public List<Synthesis> getSynthesises(){
		return synthRepo.getSynthesises();
	}

	@Override
	public List<String> processRequest(){
		List<String> statusList = Collections.synchronizedList(new ArrayList<>());
		createRequestFromRepo();
		startThread(statusList);
		runManalith(statusList);
		synthRepo.emptyRepo();
		return statusList;
	}

	private void createRequestFromRepo(){
		List<Synthesis> synthList = Collections.synchronizedList(new ArrayList<>());
		synthList = getSynthesises();
		for(Synthesis synth : synthList){
			synth.requestMana(manalith);
		}
	}

	private void startThread(List<String> statusList){
		new Thread(new Runnable(){
			public void run(){
				runManalith(statusList);
			}
		}).start();
	}

	private void runManalith(List<String> statusList){
		while(!manalith.noRequest()){
			statusList.add(manalith.processRequest());
		}
	}
}