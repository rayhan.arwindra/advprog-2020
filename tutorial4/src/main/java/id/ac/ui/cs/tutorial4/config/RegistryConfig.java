package id.ac.ui.cs.tutorial4.config;

import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;

@Configuration
public class RegistryConfig {

    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
        return registry ->  registry.config().commonTags("app.name","tutorial4");
    }
    @Bean
    TimedAspect  timedAspect(MeterRegistry  registry) {
        return  new  TimedAspect(registry);
    }


}
