<h1>Monitoring Metrics</h1>

<p>Explain what is contained in the /actuator endpoint.</p>

<p>Answer: </p>
The actuator endpoint contains information on links to all enabled actuator endpoints. Actuator endpoints helps you monitor and interact with your application.

<h1>Prometheus</h1>

<p>Try the /actuator/prometheus endpoint. Look at its contents and write what you understand about it:</p>

<p>Answer: </p>
<p>I edited the prometheus.yml file so that it connects to localhost:8080 as such:</p>
![prom](images/prom_yml.JPG)
The actuator/prometheus endpoint displays metrics data. A couple of data which I understand are:
<ul>
    <li>jvm_memory_max_bytes: Displays the maximum amount of memory in bytes for memory management</li>
    <li>process_cpu_usage: Displays the cpu usage for jvm process</li>
    <li>jvm_buffer_memory_used: Indicates the memory that jvm uses for this buffer</li>
    <li>hikaricp_connections: Displays total connections</li>
    <li>hiakricp_connections_timeout_total: Displays connection timeout total count</li>
    <li>hikaricp_connections_acquire_seconds: Time required to acquire connection</li>
</ul>

<h1>Using the @timed annotation</h1>
<p>Try adding a Timed annotation to a function, check it with Prometheus, and explain what you see.</p>
<p>Answer:</p>
I added a Timed annotation named testdb to function home in the file homecontroller.java. Then, I loaded the webpage at localhost:8000/testdb.
Then, I went to prometheus and loaded the query testdb_seconds_count, testdb_seconds_max, and testdb_seconds_sum. According to my observations, here's
what each of those queries mean:
<ul>
    <li>tesdb_seconds_count: The number of times the website is visited</li>
    ![count](images/count.JPG)
    <li>testdb_seconds_max: The longest time the website is visited</li>
    ![max](images/max.JPG)
    <li>testdb_seconds_sum: The total sum of time the website is visited</li>
    ![sum](images/sum.JPG)
</ul>
