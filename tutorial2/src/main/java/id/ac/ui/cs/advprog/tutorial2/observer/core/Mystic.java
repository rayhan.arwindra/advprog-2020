package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Mystic extends Adventurer {
    public Mystic(Guild guild){
        super(guild);
    }
    public void update(){
        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
